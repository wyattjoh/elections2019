import React, { StatelessComponent } from "react";

const format = "h:mm:ss a";

export interface Props {
  lastUpdated: Date;
  nextUpdateAt: Date;
}

const UpdatedAt: StatelessComponent<Props> = ({
  lastUpdated,
  nextUpdateAt
}) => (
  <div>
    Last Updated <b>{lastUpdated.toLocaleTimeString()}</b>.<br />
    Next Update <b>{nextUpdateAt.toLocaleTimeString()}</b>.
  </div>
);

export default UpdatedAt;
