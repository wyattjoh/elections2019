import React, { StatelessComponent } from "react";

import "./ProgressBar.css";

export interface Props {
  percent: number;
  color: string;
}

const ProgressBar: StatelessComponent<Props> = ({ percent, color }) => (
  <div className="ProgressBar">
    <span
      className="ProgressBar--progress"
      style={{
        width: percent.toFixed(1) + "%",
        backgroundColor: color
      }}
    />
  </div>
);

export default ProgressBar;
