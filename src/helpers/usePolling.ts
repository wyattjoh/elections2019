/**
 * Sourced from https://github.com/vivek12345/react-polling-hook/blob/e8cc87dea9165d27844f5ce49700cd2d79cca9f2/src/usePolling.js
 */

import React, { useState, useEffect, useRef } from "react";

export interface PollingConfig<T> extends RequestInit {
  url: string;
  start?: boolean;
  interval?: number;
  retryCount?: number;
  onSuccess: (data: T) => boolean | void;
  onFailure: (err: Error) => void;
}

function usePolling<T>(config: PollingConfig<T>) {
  const version = React.version.split("-");
  if (version[0] < "16.7.0") {
    throw new Error(
      "Hooks are only supported in React 16.7.0-alpha release or above"
    );
  }
  let {
    url,
    interval = 3000,
    retryCount = 0,
    onSuccess,
    onFailure = () => {},
    start = false,
    ...api
  } = config;
  const [isPolling, togglePolling] = useState(start);

  const persistedIsPolling = useRef<boolean>();
  const isMounted = useRef<boolean>();
  const poll = useRef<NodeJS.Timeout | null>();

  persistedIsPolling.current = isPolling;

  useEffect(() => {
    isMounted.current = true;
    startPolling();
    return () => {
      isMounted.current = false;
      stopPolling();
    };
  }, []);

  useEffect(() => {
    run();
  }, [start]);

  // if no url specified, throw an error
  if (!url) {
    throw new Error(
      "No url provided to poll. Please provide a config object with the url param set"
    );
  }

  const shouldRetry = retryCount ? true : false;

  const stopPolling = () => {
    if (isMounted.current) {
      if (poll.current) {
        clearTimeout(poll.current);
        poll.current = null;
      }
      togglePolling(false);
    }
  };

  const startPolling = () => {
    // why this does not update state?
    togglePolling(true);
    // call runPolling, which will start timer and call our api
    runPolling();
  };

  const run = () => {
    /* onSuccess would be handled by the user of service which would either return true or false
     * true - This means we need to continue polling
     * false - This means we need to stop polling
     */
    return fetch(url, api)
      .then(resp => {
        return resp.json().then(data => {
          if (resp.ok) {
            return data;
          } else {
            return Promise.reject({ status: resp.status, data });
          }
        });
      })
      .then(onSuccess);
  };

  const runPolling = () => {
    const timeoutId = setTimeout(() => {
      run()
        .then((keepPolling = false) => {
          persistedIsPolling.current && keepPolling
            ? runPolling()
            : stopPolling();
        })
        .catch(error => {
          if (shouldRetry && retryCount > 0) {
            onFailure && onFailure(error);
            retryCount--;
            runPolling();
          } else {
            onFailure && onFailure(error);
            stopPolling();
          }
        });
    }, interval);
    poll.current = timeoutId;
  };

  return [isPolling, startPolling, stopPolling];
}

export default usePolling;
