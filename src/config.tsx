export default {
  interval: 5500,
  totalRidings: 82,
  parties: {
    NDP: {
      color: "rgb(246, 144, 49)"
    },
    AP: {
      color: "rgb(0, 174, 239)"
    },
    UCP: {
      color: "rgb(0, 93, 124)"
    },
    LIB: {
      color: "rgb(234, 109, 106)"
    },
    OTH: {
      color: "rgb(255, 220, 0)"
    }
  }
};
