import React, { useState } from "react";
import cn from "classnames";

import config from "./config";
import usePolling from "./helpers/usePolling";
import "./App.css";
// import UpdatedAt from "./components/UpdatedAt";
import ProgressBar from "./components/ProgressBar";
import Logo from "./images/alberta.svg";

interface Party {
  color: string;
}

type PartyCode = keyof typeof config.parties;

function getParty(code: PartyCode): Party {
  return config.parties[code];
}

interface PartyResult {
  Name_En: string;
  Name_Fr: string;
  ShortName_En: PartyCode;
  ShortName_Fr: string;
  Leading: number;
  Elected: number;
  Net: number;
  Votes: number;
  Pop: string;
  PartyID: number;
  CallData_En: string;
  CallData_Fr: string;
}

const App = () => {
  // Stash the results.
  const [results, setResults] = useState<PartyResult[]>([]);

  // Setup the active.
  const [activeTabs, setActiveTabs] = useState<Record<PartyCode, boolean>>(
    (Object.keys(config.parties) as PartyCode[]).reduce(
      (col, key: PartyCode) => ({ ...col, [key]: false }),
      {} as Record<PartyCode, boolean>
    )
  );

  // // Save some date information.
  // const [lastUpdated, setLastUpdated] = useState<Date | null>(null);
  // const [nextUpdateAt, setNextUpdateAt] = useState<Date>(
  //   new Date(Date.now() + config.interval)
  // );

  // Start polling now.
  usePolling<PartyResult[]>({
    method: "GET",
    url: "https://electionsapi.cp.org/api/alberta2019/Totals_By_Party",
    interval: config.interval,
    start: true,
    onSuccess: results => {
      setResults(results);
      // const now = Date.now();
      // setLastUpdated(new Date(now));
      // setNextUpdateAt(new Date(now + config.interval));
      return false;
    },
    onFailure: err => console.error(err)
  });

  // if (!lastUpdated) {
  //   return null;
  // }

  return (
    <div className="container">
      <h1 className="mb-4">
        <img src={Logo} alt="Alberta" className="" /> <span>2019</span>
      </h1>
      {/* <UpdatedAt lastUpdated={lastUpdated} nextUpdateAt={nextUpdateAt} /> */}
      <div className="accordion results mt-4 mb-4">
        {results
          // .map(result => ({
          //   ...result,
          //   Elected: Math.round(Math.random() * config.totalRidings)
          // }))
          .map(result => {
            const code = result.ShortName_En;
            const active = activeTabs[code];
            const party = {
              ...result,
              ...getParty(code)
            };

            return (
              <div
                className="card"
                key={party.ShortName_En}
                style={{ borderLeftColor: party.color }}
              >
                <div className="card-header">
                  <ProgressBar
                    percent={(party.Net / config.totalRidings) * 100.0}
                    color={party.color}
                  />
                  <h2 className="mb-0 d-flex justify-content-between">
                    <button
                      className={cn("btn btn-link", active ? "" : "collapsed")}
                      type="button"
                      onClick={() => {
                        setActiveTabs({ ...activeTabs, [code]: !active });
                      }}
                    >
                      {party.ShortName_En} - {party.Name_En}
                    </button>
                    <span className="small muted">
                      {party.Net}/{config.totalRidings}
                    </span>
                  </h2>
                </div>
                <div className={cn("collapse", active && "show")}>
                  <div className="card-body">
                    <table className="table table-hover table-borderless">
                      <tbody>
                        <tr>
                          <td>Popular Vote</td>
                          <td>{party.Pop}%</td>
                        </tr>
                        <tr>
                          <td>Elected</td>
                          <td>{party.Elected}</td>
                        </tr>
                        <tr>
                          <td>Lead</td>
                          <td>{party.Leading}</td>
                        </tr>
                        <tr>
                          <td>Net</td>
                          <td>{party.Net}</td>
                        </tr>
                        <tr>
                          <td>Votes</td>
                          <td>{party.Votes}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            );
          })}
      </div>
      <p className="muted">
        44 needed for majority. Data pulled from{" "}
        <a
          href="https://thecanadianpress-a.akamaihd.net/graphics/2019/alberta-election/alberta-election-map/index.html"
          target="_blank"
          rel="noopener noreferrer"
        >
          The Canadian Press
        </a>
        . Source code available on{" "}
        <a
          href="https://gitlab.com/wyattjoh/elections2019"
          target="_blank"
          rel="noopener noreferrer"
        >
          Gitlab
        </a>
        .
      </p>
    </div>
  );
};

export default App;
